# alpine-ffmpeg
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-ffmpeg)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-ffmpeg)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-ffmpeg/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-ffmpeg/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : ffmpeg
    - FFmpeg is a free software project that produces libraries and programs for handling multimedia data.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/alpine-ffmpeg:[ARCH_TAG]
```



----------------------------------------
#### Usage

* Excute `ffmpeg` in docker terminal.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

